import gevent
from gevent import pywsgi
from app import get_path
import os
import json

def handle(environ, start_response):
    start_response('200 OK', [('Content-Type', 'text/json'),
                              ('Access-Control-Allow-Origin', '*')])

    name, org_mtime = environ['PATH_INFO'].strip('/').split('/')
    org_mtime = int(org_mtime)
    path = get_path(name)

    for i in xrange(30):
        mtime = int(os.path.getmtime(path))
        if int(mtime) != org_mtime:
            break
        gevent.sleep(0.5)

    data = map(json.loads, open(path).read().splitlines())
    yield json.dumps([mtime, data])

server = pywsgi.WSGIServer(('0.0.0.0', 1234), handle)
server.serve_forever()
