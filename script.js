var mtime = 0
var path = location.pathname.split('/')
var ident = path[path.length-1]

function query() {
    $.ajax({
        url: 'http://' + location.hostname + ':1234/' + ident + '/' + mtime,
        success: function(data) {
            var vals = data[1]
            mtime = data[0]
            render(vals)
            setTimeout(query, 1000)
        },
        dataType: 'json',
        error: function() {
           setTimeout(query, 3000)
        }})
}

function render(vals) {
    var elem = $('.show')
    elem.html('')
    for(var i in vals) {
        var v = vals[i]
        $('<div class=item></div>').text(v['type'] + ': ' + v['content']).appendTo(elem)
    }
}

$(function() {
    $(".csv").attr('href', '/' + ident + '.txt')
    $(".json").attr('href', '/' + ident + '.json')
    $(".csv span").text(ident)
    query();
})
