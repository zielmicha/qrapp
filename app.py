#!/usr/bin/python
import cgi
import json
import os
from flask import Flask, request, redirect, Response
app = Flask(__name__)

data_dir = 'data'

@app.route('/')
def main():
    return redirect('https://play.google.com/store/apps/details?id=com.zielm.wbc')

@app.route('/s/<file>')
def res(file):
    if file in ('script.js', ):
        return Response(open(file).read(), mimetype='text/plain')

@app.route("/<id>.json")
def show_json(id):
    data = open(get_path(id)).read().strip()
    data = data.replace('\n', ',\n')
    return Response('[%s]' % data, mimetype='text/plain; charset=utf-8')

@app.route("/<id>.txt")
def show_txt(id):
    data = map(json.loads, open(get_path(id)).read().splitlines())

    return Response('\r\n'.join( c['content'] for c in data ), mimetype='text/plain; charset=utf-8')

@app.route("/<id>/post")
def post(id):
    with open(get_path(id), 'a') as f:
        f.write(json.dumps({'content': request.args['content'], 'type': request.args['formatName']}) + '\n')
    return 'ok'

@app.route("/<id>")
def show(id):
    codes = map(json.loads, open(get_path(id)).read().splitlines())
    data = open('template.html').read() % cgi.escape('\n'.join( code['content'] for code in codes ))
    return Response(data, mimetype='text/html; charset=utf-8')

@app.route("/favicon.ico")
def favicon():
    return ''

def get_path(id):
    path = data_dir + '/' + id.encode('hex')
    if not os.path.exists(path):
        with open(path, 'a') as f: pass
    return path

if __name__ == "__main__":
    app.run(port=7600)
