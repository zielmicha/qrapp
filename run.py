cd $(basename "$0")
(
    setsid python app.py &
    setsid python apppoll.py &
) & > data/log 2>&1 </dev/null
